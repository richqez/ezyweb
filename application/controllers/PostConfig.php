<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PostConfig extends CI_Controller { 

    public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
    }
    
    public function index($output = null)
	{
        $crud = new grocery_CRUD();
        $crud->unset_add();
        $crud->unset_delete();
		$crud->set_table('post_config');
        $crud->set_relation('section_1','post_category','post_category_name');
        $crud->set_relation('section_2','post_category','post_category_name');
        $crud->set_relation('section_3','post_category','post_category_name');
        $output = $crud->render();
        
       $this->load->view('backend/master',[
           'crud' => $output,
           'title' => 'ประเภทของโพส'
       ]);
    }


    
}