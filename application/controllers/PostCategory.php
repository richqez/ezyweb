<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PostCategory extends CI_Controller { 

    public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
    }
    
    public function index($output = null)
	{
        $crud = new grocery_CRUD();
		
		$crud->set_table('post_category');

        $output = $crud->render();
        
       $this->load->view('backend/master',[
           'crud' => $output,
           'title' => 'ประเภทของโพส'
       ]);
    }


    
}