<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Controller { 

    public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
    }
    
    public function index($output = null)
	{
        $crud = new grocery_CRUD();
		
        $crud->set_table('post');
        $crud->set_relation('post_category_id','post_category','post_category_name');
        
        $crud->set_field_upload('post_img_thumbnail','assets/uploads/files');

        $crud->display_as('post_topic','หัวข้อ');
        $crud->display_as('post_img_thumbnail','รูปตัวอย่าง');
        $crud->display_as('post_keyword','คำค้น');
        $crud->display_as('post_category_id','หมวดหมู่');

        $crud->unset_add_fields('created_date');
        $crud->unset_columns('post_content');

        $output = $crud->render();
        
       $this->load->view('backend/master',[
           'crud' => $output,
           'title' => 'โพส'
       ]);
    }


    
}