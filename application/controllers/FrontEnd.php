<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontEnd extends CI_Controller { 

    public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		
    }
    
    public function index()
	{

        // find sectionConfig
        $post_config = $this->db->get('post_config')->row();
        $section1 = $this->db->get_where('post',['post_category_id' => $post_config->section_1])->result();
        $section2 = $this->db->get_where('post',['post_category_id' => $post_config->section_2])->result();
        $section3 = $this->db->get_where('post',['post_category_id' => $post_config->section_3])->result();
        
        $section1_tilte = $this->db
            ->get_where('post_category',['post_category_id' => $post_config->section_1])
            ->row()
            ->post_category_name;
        
        $section2_tilte = $this->db
            ->get_where('post_category',['post_category_id' => $post_config->section_2])
            ->row()
            ->post_category_name;

        $section3_tilte = $this->db
            ->get_where('post_category',['post_category_id' => $post_config->section_3])
            ->row()
            ->post_category_name;

        $this->load->view('frontend/template1/master',[
            'templateUrl' => base_url() . 'assets/frontend/template1/' ,
            'title' => 'ประเภทของโพส',
            'posts' => [
                'section1_title' => $section1_tilte, 
                'section1' => $section1,
                'section_2title' => $section2_tilte, 
                'section2' => array_chunk($section2,3),
                'section3_title' => $section3_tilte, 
                'section3' => $section3
            ]
        ]);
    }


    
}